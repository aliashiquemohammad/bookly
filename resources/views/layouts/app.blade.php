<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->

    <!-- Custom fonts for this template -->
    
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset( 'css/app.css' )}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset( 'css/bootstrap.min.css' )}}"/>
    
</head>  
<body id="page-top">

    <!-- Navigation -->
    @include( 'includes.nav' )

    <!-- Header -->
    @include( 'includes.header' )

    <!-- Content -->
    @yield( 'content' )

    <!-- Footer -->
    @include( 'includes.footer' )

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset( 'js/jquery.min.js' )}}"></script>
    <script src="{{asset( 'js/bootstrap.min.js' )}}"></script>

    <!-- Plugin JavaScript -->

    <!-- Custom scripts for this template -->
    <script src="{{ asset('js/app.js') }}" defer></script>

  </body>
</body>
</html>